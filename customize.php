<?php

	abstract class Helper_Customize{

		public $customize;
		
		public $panels = array();
		public $sections = array();
		public $controls = array();
		public $settings = array();

		public $styles = array('controls' => array(), 'preview' => array());
		public $scripts = array('controls' => array(), 'preview' => array());

		final function __construct(){
			add_action('customize_register', array($this, 'register'));
			add_action('customize_preview_init', array($this, 'load_preview_scripts'));
			add_action('customize_controls_enqueue_scripts', array($this, 'load_control_scripts'));
			add_action('customize_save_after', array($this, 'initialize_mod_defaults'));
		}

		abstract function customization_page();

		public function add_panel($id, $args = array()){
			$this->panels = is_string($id)? 
				array_merge(array('id' => $id), $args):
				array_merge($this->panels, $id); 
		}

		public function add_section($id, $args = array()){
			$this->sections = is_string($id)? 
				array_merge(array('id' => $id), $args):
				array_merge($this->sections, $id); 
		}

		public function add_setting($id, $args = array()){
			$this->settings = is_string($id)? 
				array_merge(array('id' => $id), $args):
				array_merge($this->settings, $id);
		}

		public function add_control($id, $args = array()){

			$controls = ($arr = is_array($id))? $id: array_merge(array('id' => $id), $args);
			
			if($arr){
				foreach($controls as $key => $control){
	
					if(!empty($control['settings']) && is_array($control['settings'][0])){
						
						if(!isset($control['settings'][0]['id'])):
							$control['settings'][0]['id'] = $control['id'];
						endif;

						$this->add_setting($control['settings']);
						
						$settings = $control['settings'];
						$controls[$key]['settings'] = array();
						
						foreach($settings as $setting){
							$controls[$key]['settings'][] = $setting['id'];
						}
						
					}

				}
			}
			
			$this->controls = array_merge($this->controls, $controls);
		}

		public function register($customize){
			
			$this->customize = $customize;
			$this->customization_page();

			foreach($this->panels as $panel){

				$id = $panel['id'];
				$title = $panel['title'];
				$description = $panel['title'];

				unset($panel['id'], $panel['title']);
				$this->customize->add_panel($id, array_merge(array(
					'title' => _x($title, 'Customize panel title'),
					'description' => _x($description, 'Customize panel Description')
				), $panel));

			}

			foreach($this->sections as $sec){
				// print_r($sec);
				$id = $sec['id'];
				$title = $sec['title'];

				unset($sec['id'], $sec['title']);
				$this->customize->add_section($id, array_merge(array('title' => _x($title, 'Customize section title')), $sec));

			}

			foreach($this->settings as $setting){

				$id = $setting['id'];
				unset($setting['id']);
				$this->customize->add_setting($id, $setting);
			}
			
			foreach($this->controls as $control){

				$id = $control['id'];
				$type = $control['type'] ?? '';
				$label = empty($control['label']) ? $control['title'] : $control['label'];
				$className = 'WP_Customize_'.ucfirst($type).'_Control';
				$choices = empty($control['choices']) ? null : $control['choices'];

				unset($control['label'], $control['title'], $control['id']);

				switch($type){
					case 'color_input':
					case 'media':
					case 'upload':
						$this->customize->add_control(new $className($this->customize, $id, array_merge(array(
							'label' => $label
						), $control)));
						break;
					case 'select':
						$this->customize->add_control($id, array_merge(array(
							'label' => _x($label, 'Customize control label'), 
							'type' => $type,
							'choices' => $choices
						), $control));
						break;
					case 'combobox':
						$this->customize->add_control(new Customize_ComboBox_Control($this->customize, $id, array_merge(array(
							'label' => $label,
							'choices' => $choices
						), $control)));
						break;
					default:
						$this->customize->add_control($id, array_merge(array('label' => _x($label, 'Customize control label'), 'type' => $type), $control));
				}
				// $this->customize->add_control($id, array_merge(['label' => _x($label, 'Customize control label')], $control));


			}

			$this->initialize_mod_defaults();

		}


		private function enqueue_style( $handle, $src = '', $deps = array(), $ver = false, $media = 'all', $in_footer = 'controls' ){
			
			if(in_array($in_footer, array('controls', 'preview'))){

				$this->styles[$in_footer][$handle] = (object) array(
					'src' => $src,
					'deps' => $deps,
					'ver' => $ver,
					'enqueue' => true
				);

			}      

		}

		private function enqueue_script( $handle, $src = '', $deps = array(), $ver = false, $in_footer = 'controls' ){
			
			if(in_array($in_footer, array('controls', 'preview'))){

				$this->scripts[$in_footer][$handle] = (object) array(
					'src' => $src,
					'deps' => $deps,
					'ver' => $ver,
					'enqueue' => true
				);

			}            
		}

		public function load_preview_scripts($id){

			foreach($this->styles['preview'] as $key => $style){

				wp_enqueue_style($key, $style['src'], $style['deps'], $style['ver']);

			}


			foreach($this->scripts['preview'] as $key => $script){

				wp_enqueue_script($key, $script['src'], $script['deps'], $script['ver'], true);

			}

		}
		
		public function load_control_scripts($id){

			wp_enqueue_style('wp-color-picker'); 
			wp_enqueue_script('wp-color-picker');

			foreach($this->styles['controls'] as $key => $style){

				wp_enqueue_style($key, $style['src'], $style['deps'], $style['ver']);

			}

			foreach($this->scripts['controls'] as $key => $script){

				wp_enqueue_script($key, $script['src'], $script['deps'], $script['ver'], true);

			}

		}
		
		/**
		 *	Appends unit (rem) to theme mod value
		 */

		public static function sanitize_callback_rem($callback){
			return sanitize_text_field($callback).'rem';
		}

		/**
		 *	Appends unit (em) to theme mod value
		 */

		public static function sanitize_callback_em($callback){
			return sanitize_text_field($callback).'em';
		}

		/**
		 * Initializes theme mod defaults
		 */

		public function initialize_mod_defaults() {

			$mods = get_theme_mods();

			$setting_set = self::recursivelyFormatSettings($this->settings);


			foreach($setting_set as $id => $set){

				if(isset($mods[$id])) continue;

				set_theme_mod($id, $set);

			}

		}

		private static function sanitize_mod_default($setting){

			return isset($setting['sanitize_callback'])? 
				$setting['sanitize_callback']($setting['default'])
			:$setting['default'];


		}

		/**
		 * 
		 * 	Recursivily formats an array of theme mod settings, returning an array of properly formatted theme mod settings.
		 * 
		 * 
		 * 	@param array $settings Takes array of settings with default values, if setting has no default value it'll be skipped
		 * 	@return array 
		 * 
		 */

		private static function recursivelyFormatSettings($settings, $value = null){

			$ret = [];
			
			foreach ($settings as $setting) {
				
				if(empty($setting['default'])) continue;

				$keys = explode('[', str_replace(']', '', $setting['id']));

				$setting['default'] = self::sanitize_mod_default($setting);
				
				$setting = self::makeArray($keys, $setting['default']);

				$ret = array_replace_recursive(
					$setting
				, $ret);
				
			}

			
			return $ret;

		}

		private static function makeArray($keys, $value){
			
			$ret = [];   
			$index = array_shift($keys);
			
			$ret[$index] = isset($keys[0])? self::makeArray($keys,$value): $value;

			return $ret;
			
		}

	}

?>