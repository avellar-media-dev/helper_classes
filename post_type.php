<?php

	if(!class_exists('Custom_Post_Type')){

		class Custom_Post_Type{

			public $name;
			public $title;
			public $args;
			public $labels;
	
			/**
			 *	Constructor: Creates new post type.
			 * 
			 * @param string $name The name of the new post type.
			 * @param array $args List of arguments for the post type.
			 * @param array $labels An array of labels for the post type.
			 * 
			 * @return void
			 */
	
			public function __construct($name, $args = array(), $labels = array()){
	
				// Set some important variables
				$this->title = $name;
				$this->name = self::uglify($name);
				$this->args = $args;
				$this->labels  = $labels;
				 
				// Add action to register the post type, if the post type does not already exist
				if( ! post_type_exists( $this->name ) )
				{
					add_action( 'init', array( &$this, 'register_post_type' ) );
				}
				 
				// Listen for the save post hook
				$this->save();
	
			}
	
			public static function uglify($string){
				return strtolower(strtr(str_replace(' ','_',$string), array(
					"á" => "a",
					"é" => "e",
					"í" => "i",
					"ó" => "o",
					"ú" => "u",
					"à" => "a",
					"è" => "e",
					"ì" => "i",
					"ò" => "o",
					"ù" => "u",
					"â" => "a",
					"ê" => "e",
					"î" => "i",
					"ô" => "o",
					"û" => "u",
					"ã" => "a",
					"õ" => "o",
					"ç" => "c"
				)));
			}

			/**
			 *  Registers the new post type.
			 * 
			 *  @return void
			 */

			public function register_post_type(){
				
				//Capitilize the words and make it plural
				$name       = $this->title;
				$plural     = $name . 's';
				 
				// We set the default labels based on the post type name and plural. We overwrite them with the given labels.
				$labels = array_merge(
				 
					// Default
					array(
						'name'                  => _x( $plural, 'post type general name' ),
						'singular_name'         => _x( $name, 'post type singular name' ),
						'add_new'               => _x( 'Add New', strtolower( $name ) ),
						'add_new_item'          => __( 'Add New ' . $name ),
						'edit_item'             => __( 'Edit ' . $name ),
						'new_item'              => __( 'New ' . $name ),
						'all_items'             => __( 'All ' . $plural ),
						'view_item'             => __( 'View ' . $name ),
						'search_items'          => __( 'Search ' . $plural ),
						'not_found'             => __( 'No ' . strtolower( $plural ) . ' found'),
						'not_found_in_trash'    => __( 'No ' . strtolower( $plural ) . ' found in Trash'), 
						'parent_item_colon'     => '',
						'menu_name'             => $plural
					),
					 
					// Given labels
					$this->labels
					 
				);
				 
				// Same principle as the labels. We set some defaults and overwrite them with the given arguments.
				$args = array_merge(
				 
					// Default
					array(
						'label'                 => $plural,
						'labels'                => $labels,
						'public'                => true,
						'show_ui'               => true,
						'supports'              => array( 'title', 'editor' ),
						'show_in_nav_menus'     => true,
						'show_in_rest'	        => true,
						'_builtin'              => false,
					),
					 
					// Given args
					$this->args
					 
				);
				 
				// Register the post type
				register_post_type( $this->name, $args );
			}
	
			/**
			 * Adds taxonomy to custom post type.
			 * 
			 * @param string $name Taxonomy name.
			 * @param array $args Arguments for registering a taxonomy.
			 * @param array $labels List of labels for the taxonomy.
			 * 
			 * @return void
			 */

			public function add_taxonomy($name,$args = array(), $labels = array()){
				if(!empty( $name )){
					
					$name = $this->name; // Saves post name for use in init action
			
					// Taxonomy properties
					$taxonomy_name      = strtolower( str_replace( ' ', '_', $name ) );
					$taxonomy_labels    = $labels;
					$taxonomy_args      = $args;
			
					// Checks if taxonomy exists and registering it for the post type if so.
					if(taxonomy_exists($name)){
					
						add_action( 'init',
							function() use( $taxonomy_name, $name )
							{
								register_taxonomy_for_object_type( $taxonomy_name, $name );
							}
						);
	
						return;
	
					}					

					//Capitilize the words and make it plural
					$name       = ucwords( str_replace( '_', ' ', $name ) );
					$plural     = $name . 's';
					
					// Default labels, overwrite them with the given labels.
					$labels = array_merge(
					
						// Default
						array(
							'name'                  => _x( $plural, 'taxonomy general name' ),
							'singular_name'         => _x( $name, 'taxonomy singular name' ),
							'search_items'          => __( 'Search ' . $plural ),
							'all_items'             => __( 'All ' . $plural ),
							'parent_item'           => __( 'Parent ' . $name ),
							'parent_item_colon'     => __( 'Parent ' . $name . ':' ),
							'edit_item'             => __( 'Edit ' . $name ),
							'update_item'           => __( 'Update ' . $name ),
							'add_new_item'          => __( 'Add New ' . $name ),
							'new_item_name'         => __( 'New ' . $name . ' Name' ),
							'menu_name'             => __( $name ),
						),
					
						// Given labels
						$taxonomy_labels
					
					);
					
					// Default arguments, overwritten with the given arguments
					$args = array_merge(
					
						// Default
						array(
							'label'                 => $plural,
							'labels'                => $labels,
							'public'                => true,
							'show_ui'               => true,
							'show_in_nav_menus'     => true,
							'_builtin'              => false,
						),
					
						// Given
						$taxonomy_args
					
					);
					
					// Add the taxonomy to the post type
					add_action( 'init',
						function() use( $taxonomy_name, $name, $args )
						{
							register_taxonomy( $taxonomy_name, $name, $args );
						}
					);

				}
			}

			/**
			 * Adds metabox to custom post type.
			 * 
			 * @param string $title Metabox title.
			 * @param array $fields Fields to be registered to the metabox.
			 * @param string $context The context within the screen where the boxes should display. 
			 * @param string $priority The priority within the context where the boxes should show ('high', 'low').
			 * 
			 * @return void
			 */

			public function add_meta_box($title, $fields = array(), $context = 'normal', $priority = 'default'){

				if(!empty($title)){

					// We need to know the Post Type name again
					$name = $this->name;
			
					// Meta variables
					$box_id         = self::uglify($title);
					$box_title      = ucwords( str_replace( '_', ' ', $title ) );
					$box_context    = $context;
					$box_priority   = $priority;
					
					// Make the fields global
					global $custom_fields;
					$custom_fields[$title] = $fields;
					
					add_action( 'admin_init', function() use( $box_id, $box_title, $name, $box_context, $box_priority, $fields ){
						add_meta_box(
							$box_id,
							$box_title,
							function($post, $data){
								global $post;
								
								// Nonce field for some validation
								wp_nonce_field( plugin_basename( __FILE__ ), 'custom_post_type' );
								
								// Get all inputs from $data
								$custom_fields = $data['args'][0];
								
								// Get the saved values
								$meta = get_post_custom( $post->ID );
								
								// Check the array and loop through it
								if( ! empty( $custom_fields ) )
								{
									/* Loop through $custom_fields */
									foreach( $custom_fields as $label => $type ){
										
										$field_id_name  = self::uglify($data['id']).'_'.self::uglify($label);

										$field = $type;
										$type = is_string($field)? $field: $field['type'];
									
										?>
											<label for="<?php echo  $field_id_name ?>"><?php echo _x($label, 'Field name') ?></label>
											<input type="<?php echo $type ?>" name="custom_meta[<?php echo $field_id_name ?>]" id="<?php echo $field_id_name ?>" value="<?php echo $meta[$field_id_name][0] ?>" />
											<?php if(!empty($field['description'])): ?>
												<small><?php echo $field['description'] ?></small>
											<?php endif; ?>
										<?php
									}
								}
							
							},
							$name,
							$box_context,
							$box_priority,
							array( $fields )
						);
					});
				}
				
			}

			/**
			 * Handles the saving of post type metaboxes.
			 * 
			 * @return void
			 */
	
			public function save(){

				// Need the post type name again
				$name = $this->name;

				add_action( 'save_post',
					function() use( $name )
					{
						// Deny the WordPress autosave function
						if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return;

						if (empty($_POST['custom_post_type']) || ! wp_verify_nonce( $_POST['custom_post_type'], plugin_basename(__FILE__) ) ) return;
					
						global $post;
						
						if( isset( $_POST ) && isset( $post->ID ) && get_post_type( $post->ID ) == $name )
						{
							global $custom_fields;
							
							// Loop through each meta box
							foreach( $custom_fields as $title => $fields )
							{
								// Loop through all fields
								foreach( $fields as $label => $type )
								{
									$field_id_name  = self::uglify($title).'_'.self::uglify($label);
									
									update_post_meta( $post->ID, $field_id_name, $_POST['custom_meta'][$field_id_name] );
								}
							
							}
						}
					}
				);

			}
	
		}

	}
?>